<?php

namespace App;

use Exception;
use PDO;

class PeopleList
{
    private array $peopleIds = [];

    /**
     * @param string|null $name
     * @param string|null $surname
     * @param string|null $birthDate
     * @param int|null $gender
     * @param string|null $birthCity
     * @throws Exception
     */
    public function __construct(
        ?string $name = null,
        ?string $surname = null,
        ?string $birthDate = null,
        ?int $gender = null,
        ?string $birthCity = null
    ) {
        if (!class_exists('App\People')) {
            throw new Exception('People class is missing!');
        }

        $db = new MyPDO();
        $query = "SELECT id FROM people WHERE 1=1";

        if (!empty($name)) {
            $query .= " AND name='$name'";
        }
        if (!empty($surname)) {
            $query .= " AND surname='$surname'";
        }
        if (!empty($birthDate)) {
            $op = '=';
            if (str_contains($birthDate, '>')) {
                $op = '>';
                $birthDate = str_replace('>', '', $birthDate);
            } elseif (str_contains($birthDate, '<')) {
                $op = '<';
                $birthDate = str_replace('<', '', $birthDate);
            } elseif (str_contains($birthDate, '!=')) {
                $op = '!=';
                $birthDate = str_replace('!=', '', $birthDate);
            }
            $query .= " AND birth_date$op'$birthDate'";
        }
        if (!empty($gender)) {
            $query .= " AND gender='$gender'";
        }
        if (!empty($birthCity)) {
            $query .= " AND birth_city='$birthCity'";
        }
        $result = $db->query($query, PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $this->peopleIds[] = $row['id'];
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getPeople(): array
    {
        $people = [];

        foreach ($this->peopleIds as $id) {
            $people[] = new People($id);
        }

        return $people;
    }

    /**
     * @throws Exception
     */
    public function deletePeople(): void
    {
        foreach ($this->peopleIds as $id) {
            $person = new People($id);
            $person->delete();
        }
    }
}