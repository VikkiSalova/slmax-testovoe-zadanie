<?php

namespace test;

use DateTime;

class Validator
{
    /**
     * @param string $name
     * @return bool|int
     */
    public static function validateName(string $name): bool|int
    {
        return preg_match('/^[a-zA-Z]+$/u', $name);
    }

    /**
     * @param string $surname
     * @return bool|int
     */
    public static function validateSurname(string $surname): bool|int
    {
        return preg_match('/^[a-zA-Z]+$/u', $surname);
    }

    /**
     * @param string $birthDate
     * @return bool
     */
    public static function validateBirthDate(string $birthDate): bool
    {
        $today = new DateTime();
        $date = DateTime::createFromFormat('Y-m-d', $birthDate);
        return $date !== false && $date <= $today;
    }

    /**
     * @param int $gender
     * @return bool
     */
    public static function validateGender(int $gender): bool
    {
        return $gender === 0 || $gender === 1;
    }

    /**
     * @param string $birthCity
     * @return bool|int
     */
    public static function validateBirthCity(string $birthCity): bool|int
    {
        return preg_match('/^[a-zA-Z]+$/u', $birthCity);
    }
}