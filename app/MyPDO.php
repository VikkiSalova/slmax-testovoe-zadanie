<?php

namespace App;

use PDO;

class MyPDO extends PDO
{
    /** @var string */
    private const SQL = 'mysql';
    /** @var string */
    private const HOST = 'localhost';
    /** @var string */
    private const DATABASE = 'test_task';
    /** @var string */
    private const USERNAME = 'root';
    /** @var string */
    private const PASSWORD = '';

    public function __construct()
    {
        $dsn = sprintf('%s:host=%s;dbname=%s', self::SQL, self::HOST, self::DATABASE);

        parent::__construct($dsn, self::USERNAME, self::PASSWORD);
    }
}