<?php

namespace App;

use Exception;
use stdClass;

class People
{
    private MyPDO $db;

    private ?int $id;
    private ?string $name;
    private ?string $surname;
    private ?string $birthDate;
    private ?int $gender;
    private ?string $birthCity;

    /**
     * @param int|null $id
     * @param string|null $name
     * @param string|null $surname
     * @param string|null $birthDate
     * @param int|null $gender
     * @param string|null $birthCity
     * @throws Exception
     */
    public function __construct(
        ?int $id = null,
        ?string $name = null,
        ?string $surname = null,
        ?string $birthDate = null,
        ?int $gender = null,
        ?string $birthCity = null
    ) {
        $this->db = new MyPDO();

        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->birthDate = $birthDate;
        $this->gender = $gender;
        $this->birthCity = $birthCity;

        if ($id !== null) {
            $this->getById($id);
        } else {
            $this->validate();
            $this->save();
        }
    }

    public function save()
    {
        $stmt = $this->db->prepare(
            'INSERT INTO people (name, surname, birth_date, gender, birth_city) 
                   VALUES (:name, :surname, :birth_date, :gender, :birth_city)'
        );
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':surname', $this->surname);
        $stmt->bindParam(':birth_date', $this->birthDate);
        $stmt->bindParam(':gender', $this->gender);
        $stmt->bindParam(':birth_city', $this->birthCity);
        $stmt->execute();
    }

    public function delete()
    {
        $stmt = $this->db->prepare('DELETE FROM people WHERE id=:id');
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();
    }

    /**
     * @param string $birthDate
     * @return int
     */
    public static function ageFromBirthDate(string $birthDate): int
    {
        $diff = date_diff(date_create($birthDate), date_create());

        return $diff->y;
    }

    /**
     * @param int $gender
     * @return string
     */
    public static function genderFromBinary(int $gender): string
    {
        return ($gender == 0) ? 'муж' : 'жен';
    }

    /**
     * @param bool $age
     * @param bool $gender
     * @return stdClass
     */
    public function format(bool $age = false, bool $gender = false): stdClass
    {
        $person = new stdClass();
        $person->id = $this->id;
        $person->name = $this->name;
        $person->surname = $this->surname;
        $person->birthDate = $this->birthDate;
        $person->gender = $this->gender;
        $person->birthCity = $this->birthCity;

        if ($age) {
            $person->age = self::ageFromBirthDate($this->birthDate);
        }

        if ($gender) {
            $person->gender = self::genderFromBinary($this->gender);
        }

        return $person;
    }

    /**
     * @param int $id
     * @return void
     */
    private function getById(int $id): void
    {
        $stmt = $this->db->prepare('SELECT * FROM people WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($data) {
            $this->id = $data['id'];
            $this->name = $data['name'];
            $this->surname = $data['surname'];
            $this->birthDate = $data['birthDate'];
            $this->gender = $data['gender'];
            $this->birthCity = $data['birthCity'];
        }
    }

    /**
     * @throws Exception
     */
    private function validate()
    {
        if (
            !Validator::validateName($this->name) ||
            !Validator::validateSurname($this->surname) ||
            !Validator::validateBirthDate($this->birthDate) ||
            !Validator::validateGender($this->gender) ||
            !Validator::validateBirthCity($this->birthCity)
        ) {
            throw new Exception("Data is not valid");
        }
    }
}