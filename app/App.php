<?php

namespace App;

use Exception;

$loader = require __DIR__ . '\..\vendor\autoload.php';
$loader->addPsr4('app\\', __DIR__);

new People(null, 'John', 'Smith', '2000-01-28', 0, 'Fila');
new People(null, 'John', 'Doy', '2000-02-28', 0, 'Fila');
new People(null, 'Mary', 'Smith', '2003-02-28', 1, 'Alaska');

try {
    new People(null, 'Mary', 'Smith', '2003-02-28', 1, 'Alaska');
} catch (Exception $exception) {
    echo $exception->getMessage();
}

try {
    $smithPeople = new PeopleList(null, 'Smith');
    var_dump('People with surname Smith:', $smithPeople);

    $youngPeople = new PeopleList(null, null, '>2000-02-01');
    var_dump('People were born after 1 March 2000:', $youngPeople);
} catch (Exception $exception) {
    echo $exception->getMessage();
}
